# Github Svelte

Projeto de demonstração como realizar um teste unitário e integrado com Svelte consumindo a API do Github.

### Técnologias

- Svelte
- Jest
- Testing Library
- Tailwind

### Iniciando

Instale todas as dependências do projeto

```sh
$ yarn install
```

### Rodando local

Para rodar o projeto local

```sh
$ yarn dev
```

### Rodando o teste local com o relatório de cobertura


```sh
$ yarn coverage
```

### Rodando o teste local

```sh
$ yarn test --watch
```