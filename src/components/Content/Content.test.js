import '@testing-library/jest-dom/extend-expect'
import { render, screen } from '@testing-library/svelte'

import Content from './Content'

test('Show render page', () => {
  render(Content)
  expect(screen.getByTestId('root')).toBeInTheDocument()
})
