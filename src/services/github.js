export const URL = 'https://api.github.com'

export const allUsers = async (page = 1) => {
  const _result = await fetch(`${URL}/users?page=${page}`)
  const _usersData = await _result.json()

  const _users = await Promise.all(
    _usersData.map(async (item) => {
      const _dataUser = await getByUser(item.login)
      return {
        ..._dataUser,
        ...item,
      }
    })
  )

  return _users
}

export const getByUser = async (login) => {
  const result = await fetch(`${URL}/users/${login}`)
  const data = await result.json()

  return data
}
