import { render, screen } from '@testing-library/svelte'

import ListItem from './ListItem';

describe('List', () => {
  it('Should render List Items', async () => {
    createComponent();
    expect(screen.getByText(/Antartida/i)).toBeInTheDocument();
  });

  it('Should render specify User', () => {
    const user = {
      html_url: 'xpto',
      name: 'Renato Oda',
      login: 'oda2',
      location: 'Florianópolis'
    };

    createComponent({ user });
    expect(screen.queryByText(/Renato Oda/i)).toBeInTheDocument();
  });
});

const createComponent = (props = {}) => {
  const defaultProps = {
    user: {
      html_url: '',
      name: 'Pingu',
      login: 'pingu.login',
      location: 'Antartida',
      blog: ''
    },
    ...props
  };

  return render(ListItem, { ...defaultProps });
};
